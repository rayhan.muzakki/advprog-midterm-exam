package id.ac.ui.cs.advprog.midterm;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MidtermProgrammingExamApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	public void main(){
		MidtermProgrammingExamApplication.main(new String[] {});
	}

}
