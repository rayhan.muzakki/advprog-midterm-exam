package id.ac.ui.cs.advprog.midterm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages={"id.ac.ui.cs.advprog.midterm"})
@EnableJpaRepositories(basePackages="id.ac.ui.cs.advprog.midterm.repositories")
@EnableTransactionManagement
@EntityScan(basePackages="id.ac.ui.cs.advprog.midterm.entities")
public class MidtermProgrammingExamApplication {

	public static void main(String[] args) {
		SpringApplication.run(MidtermProgrammingExamApplication.class, args);
	}

}
