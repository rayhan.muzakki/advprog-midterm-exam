package id.ac.ui.cs.advprog.midterm.repositories;


import java.util.List;

import id.ac.ui.cs.advprog.midterm.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    List<User> findByName(String name);

}
